# DEPRECATED
Please note that this unofficial version is now deprecated, as an official version is available at [this webpage](https://verwaltungsportal.uni-oldenburg.de/).

# Beamer Theme UOL SigProc
Unofficial LaTeX Beamer Theme for Signal Processing Group, University of Oldenburg (UOL)
## Example of Title
![Example of Title](https://gitlab.uni-oldenburg.de/hura4843/beamer-theme-uol-sigproc/-/raw/master/example_images/example_title.png)

## Example of Slide
![Example of Slide](https://gitlab.uni-oldenburg.de/hura4843/beamer-theme-uol-sigproc/-/raw/master/example_images/example_slide.png)
